#!/usr/bin/env python3
import argparse
import pandas as pd

FSTR = "%d.%m.%y %H:%M"

# Usage
# ---------
# Dependencies:
# * pandas
# can usually be installed with "pip install pandas",
# check docs https://pandas.pydata.org/pandas-docs/stable/install.html#installing-from-pypi
#
# Command line:
# python3 uvalue.py file.csv [optional: --start "23.03.18 10:30" and/or --end "26.03.18 05:30"]
#
# In Python:
# from uvalue import GomsData  # import
# measurement = GomsData('file.csv')  # init; file.csv is downloaded from gOMS portal v2 via "Export to CSV")
# print(measurement)  # print summary
# measurement.set_time(start='23.03.18 10:30', end='26.03.18 05:30')  # Adjust the "slicer" to a different start & end date
# print(measurement)  # print summary again


class GomsData():
    """ Main class for reading in .csv source file and calculating U/R-Value and other values """
    def __init__(self, filename, start=None, end=None, no_zeros=False):
        self.df = self._read_data(filename, no_zeros)
        self._bin_length = self.df.index[1] - self.df.index[0]
        self.set_time(start=start, end=end)

    def _read_data(self, filename, no_zeros=False):
        """ Function to convert raw .csv to a Pandas dataframe, set index and clean the data """
        df = pd.read_csv(filename, parse_dates=[0], dayfirst=True)

        if no_zeros:
            try:
                df.set_index("Date & Time", inplace=True, verify_integrity=True)
            except KeyError:
                pass
            df.set_index("Timestamp", inplace=True, verify_integrity=True)

            # this is the "old way" of calculation:
            # just cut out data points which do not contain all necessary raw data -> sometimes not correct
            return df[df.notna().all(axis=1)]

        # the "new way" of calculation:
        # in case of incomplete data, fill complete data point with zeros and carry on as usual
        mask = df.iloc[:, 1:6].isna().any(axis=1)  # mask is True for incomplete data points
        end = (~mask[::-1]).idxmax()  # find last complete data point
        df.loc[mask, 1:6] = 0  # set HF & all temperatures to 0 where mask is True
        df = df.loc[:end, :]  # disregard incomplete values at the end

        # backwards compatibility: column name can be either "Date & Time" or "Timestamp"
        # if neither -> Exception
        try:
            df.set_index("Date & Time", inplace=True, verify_integrity=True)
        except KeyError:
            pass
        df.set_index("Timestamp", inplace=True, verify_integrity=True)

        return df

    def _str_to_column(self, string):
        """ For backwards compatibility, this function accounts for different
        column naming in the source .csv file.
        It searches for a fitting column by given string, e.g. 'hf'. """
        candidates = []
        for col in self.df.columns:
            if string in col or string.replace('_', '') in col:
                candidates.append(col)
        if not candidates or len(candidates) > 1:
            raise ValueError
        return self.df[candidates[0]]

    def set_time(self, start=None, end=None):
        """ This defines time ranges needed for calculation,
        can be called manually to emulate time slicer setting """

        # if start or end is given, convert it to datetime. else use min/max timestamp of data
        self._start = pd.to_datetime(start, dayfirst=True) if start else self.df.index.min()
        self._end = pd.to_datetime(end, dayfirst=True) if end else self.df.index.max()

        self._start_analysis = self._end - pd.DateOffset(days=int(self.duration())) + self._bin_length
        self._end_R23s = self._start_analysis + pd.DateOffset(days=int(self.duration() * 2 / 3)) - self._bin_length
        self._start_R23e = self._end - pd.DateOffset(days=int(self.duration() * 2 / 3)) + self._bin_length
        self._end_R24 = self._end - pd.DateOffset(days=1)

    def _calc_avg(self, df_column, start, end):
        """ Calculate avg value of given column for dates between (and including) start & end """
        if isinstance(df_column, str):
            df_column = self._str_to_column(df_column)
        return df_column[start:end].mean()

    def U(self, start=None, end=None):
        """ Calculate U value for dates between (and including) start & end
        default value for start/end: min/max timestamp of data """
        start = start or self._start
        end = end or self._end
        T_i = self._calc_avg('T_i', start, end)
        T_e = self._calc_avg('T_e', start, end)
        hf = self._calc_avg('HF', start, end)
        return hf / (T_i - T_e)

    def R(self, start=None, end=None):
        """ Calculate R value for dates between (and including) start & end
        default value for start/end: min/max timestamp of data """
        start = start or self._start
        end = end or self._end
        T_si = self._calc_avg('T_si', start, end)
        T_se = self._calc_avg('T_se', start, end)
        hf = self._calc_avg('HF', start, end)
        return (T_si - T_se) / hf

    def U_analysis(self):
        return self.U(start=self._start_analysis)

    def _R_analysis(self):
        return self.R(start=self._start_analysis)

    def _R24(self):
        return self.R(start=self._start_analysis, end=self._end_R24)

    def _R23s(self):
        return self.R(start=self._start_analysis, end=self._end_R23s)

    def _R23e(self):
        return self.R(start=self._start_R23e)

    def dR23(self):
        R23s = self._R23s()
        R23e = self._R23e()
        return 2 * abs(R23s - R23e) / (R23s + R23e)

    def dR24(self):
        R_analysis = self._R_analysis()
        R24 = self._R24()
        return 2 * abs(R_analysis - R24) / (R_analysis + R24)

    def duration(self, start=None, end=None):
        start = start or self._start
        end = end or self._end
        # add length of one bin to account for bin "rounding" (8:40 bin includes data from 8:30+x)
        return (end - start + self._bin_length).total_seconds() / (3600 * 24)

    def __str__(self):
        out = ("\n"
               "Start time: {start}\n"
               "End time: {end}\n"
               "Measurement time [days]: {duration:.2f}\n"
               "Analysis starts at: {analysis_start}\n\n"
               "{start} to {end} - R total: {R_total:.3f}\n"
               "{analysis_start} to {end} - R analysis: {R_analysis:.3f}\n"
               "{analysis_start} to {end_R24} - R24: {R24:.3f}\n"
               "{analysis_start} to {end_R23s} - R_2/3s: {R23s:.3f}\n"
               "{start_R23e} to {end} - R_2/3e: {R23e:.3f}\n\n"
               "{analysis_start} to {end} - U analysis: {U_analysis:.3f}\n\n"
               "Total time: {duration:.2f}d (cutoff: 3d)\n"
               "dR24: {dR24:.2f}% (cutoff: 5%)\n"
               "dR_2/3: {dR23:.2f}% (cutoff: 5%)\n")

        # for displaying we subtract one bin length from every start timestamp,
        # to make the duration calculation transparent
        return out.format(start=(self._start - self._bin_length).strftime(FSTR),
                          end=self._end.strftime(FSTR),
                          duration=self.duration(),
                          analysis_start=(self._start_analysis - self._bin_length).strftime(FSTR),
                          R_total=self.R(),
                          R_analysis=self._R_analysis(),
                          end_R24=self._end_R24.strftime(FSTR),
                          R24=self._R24(),
                          end_R23s=self._end_R23s.strftime(FSTR),
                          R23s=self._R23s(),
                          start_R23e=(self._start_R23e - self._bin_length).strftime(FSTR),
                          R23e=self._R23e(),
                          U_analysis=self.U_analysis(),
                          dR24=100 * self.dR24(),
                          dR23=100 * self.dR23())


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('csvfile', help='Specify raw csv file')
    parser.add_argument('--start', '-s', help='Specify start timestamp')
    parser.add_argument('--end', '-e', help='Specify end timestamp')
    parser.add_argument('--no-zeros', '-n', action='store_true', help='"old way", don\'t fill n/a data with zeros')
    args = parser.parse_args()

    gmeas = GomsData(args.csvfile, start=args.start, end=args.end, no_zeros=args.no_zeros)
    print(gmeas)
